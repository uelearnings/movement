# Movement

This project focuses mainly on movement. It includes walking, running and jumping from a first person view. The character also plays different animations depending on the movement. 
Furthermore, the player is able to push nearby boxes away by pressing the left mouse button. 

Other than that I've tried a bit of placing objects, foliage, lighting, camera settings and landscape. 
_Please don't expect greate visuals_
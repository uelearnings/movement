// Fill out your copyright notice in the Description page of Project Settings.
#include "MainCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FPSCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	check(FPSCameraComponent != nullptr);

	Movement = Cast<UCharacterMovementComponent>(GetCharacterMovement());

	Trigger = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Trigger"));
	check(Trigger != nullptr);

	Trigger->SetupAttachment(GetRootComponent());
	Trigger->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	Trigger->SetCapsuleHalfHeight(100.0f);
	Trigger->SetCapsuleRadius(40.0f);
	Trigger->SetRelativeLocation(FVector(70.0f, 0.0f, -50.0f));
	Trigger->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
	Trigger->SetGenerateOverlapEvents(true);
	Trigger->ShapeColor = FColor::Green;
	Trigger->bHiddenInGame = true;
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	check(GEngine != nullptr);
	GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, TEXT("Main character spawned!"));

	GetMesh()->HideBoneByName("Head", EPhysBodyOp::PBO_None);

	FPSCameraComponent->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, "CameraSocket");
	FPSCameraComponent->bUsePawnControlRotation = true;

	Trigger->OnComponentBeginOverlap.AddDynamic(this, &AMainCharacter::OnOverlapBegin);
	Trigger->OnComponentEndOverlap.AddDynamic(this, &AMainCharacter::OnOverlapEnd);
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &AMainCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AMainCharacter::AddControllerPitchInput);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMainCharacter::StartSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMainCharacter::StopSprint);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMainCharacter::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AMainCharacter::StopJump);

	PlayerInputComponent->BindAction("Push", IE_Pressed, this, &AMainCharacter::PushObject);
}

void AMainCharacter::MoveForward(float value) 
{
	if (value < 0.0f) //if is walking backwards, set speed lower
	{
		Movement->MaxWalkSpeed = WalkBackSpeed;
	}
	else if(!IsSprinting)
		Movement->MaxWalkSpeed = WalkSpeed;

	ForwardAxis = value;

	FVector direction = GetRootComponent()->GetComponentTransform().GetScaledAxis(EAxis::X);;

	AddMovementInput(direction, value);
}

void AMainCharacter::MoveRight(float value) 
{
	FVector direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(direction, value);
}

void AMainCharacter::StartSprint() 
{
	IsSprinting = true;

	Movement->MaxWalkSpeed = RunSpeed;
}

void AMainCharacter::StopSprint() 
{
	IsSprinting = false;
	Movement->MaxWalkSpeed = WalkSpeed;
}

void AMainCharacter::PushObject() 
{
	if (ObjectsInRange.Num() > 0) 
	{
		UPrimitiveComponent* PushedObject = ObjectsInRange.Pop(true);

		if (PushedObject != nullptr)
		{
			FVector ObjectLocation = PushedObject->GetAttachmentRootActor()->GetTransform().GetLocation();
			FVector PlayerLocation = GetCapsuleComponent()->GetComponentTransform().GetLocation();

			FVector ImpulseVector = ObjectLocation - PlayerLocation;
			PushedObject->AddImpulse(ImpulseVector.GetSafeNormal() * 500000.0f);
		}
	}
}

void AMainCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) 
{
	ObjectsInRange.Add(OtherComp);
}

void AMainCharacter::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) 
{
	ObjectsInRange.Remove(OtherComp);
}

void AMainCharacter::StartJump() {
	bPressedJump = true;
}

void AMainCharacter::StopJump() {
	bPressedJump = false;
}


float AMainCharacter::GetForwardAxisValue() {
	return ForwardAxis;
}



// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GetHomeSafeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GETHOMESAFE_API AGetHomeSafeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

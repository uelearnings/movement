// Copyright Epic Games, Inc. All Rights Reserved.

#include "GetHomeSafe.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GetHomeSafe, "GetHomeSafe" );
